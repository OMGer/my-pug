const express = require('express');
const app = express();

app.set('views', './views');
app.set('view engine', 'pug');

app.get('/', (req, res, next) =>{
  res.render('index', {
    title: 'Hey',
    message: 'Hello!'
  })
})

app.listen(3000, () =>{
  console.log('Listening!')
})
